De fleste ledere er ikke klar over, at de har en kernekonflikt i deres organisationer.

På den ene side er der et behov for altid at gøre mere med mindre, hvilket er blevet gennemgribende inden for IT som følge af en forsimplet forståelse af Moores lov. Det skaber et miljø, der er styret af knaphed og besparelser, eller en stordriftsøkonomi, som er drevet af effektivitet. Når IT styres som et omkostningscenter i en organisation, er det fanget i denne økonomi, og som følge heraf vil en CIO udvikle sit eget sprog og sin egen organisationsforståelse.

I modsætning hertil har CTO'er en tendens til at eksistere i en økonomi, der fremmes af innovation, eksperimentering og behovet for at differentiere sig. Effektivitet alene vil ikke adskille en virksomhed fra dens konkurrenter, i stedet opnås forretningsfordele ved at tage risici og omfavne forandringer. I denne differentieringsøkonomi skal der ofte mange fejlslagne eksperimenter til for at opnå det ønskede resultat, og derfor er det effektiviteten, der er afgørende.

Konflikten opstår, når en klog COO eller CXO, der er hyperfokuseret på at forbedre aktionærernes udbytte, indser, at begge økonomier er nødvendige for succes. Toplinjeomsætningen er traditionelt drevet af differentieringsøkonomien, og omkostningsoptimering af stordriftsøkonomien. Evnen til at skifte problemfrit mellem de to styrker bundlinjen og glæder aktionærerne!

De to økonomier har dog meget forskellige leksikoner. I stordriftsøkonomien bruger vi udtryk som regulering, reduktion, genbrug, konsolidering og standardisering. I differentieringsøkonomien taler vi derimod om innovation, eksperimentering, originalitet, tilpasning og acceleration.

Hvordan ved man som CXO, hvornår og hvor man skal skifte?

Under pandemien indså Delta Airlines, at deres passagergrundlag ville blive drastisk reduceret, og de traf en strategisk beslutning om at dreje mere over på stordrift. De outsourcede en stor del af deres kundesupport og IT til Indien, fyrede medarbejdere og reducerede driftsomkostningerne. Det gjorde det muligt for dem at overleve i tider med knaphed på forretning. Da der igen kom gang i forretningen, blev de indiske callcentre overvældet næsten fra den ene dag til den anden. Flyrejser vendte tilbage i bølger i forskellige dele af verden som USA, mens Indien var midt i den værste coronavirus-variant af pandemien. Noget måtte ændres for at genvinde kundetilfredsheden.

Amazon havde en meget anderledes oplevelse under pandemien. Jeff Bezos indså, at de havde en mulighed, og begyndte at investere mere i forsyningskæden, ansætte flere lagerarbejdere og øge antallet af hjemmeleveringer for at imødekomme den øgede efterspørgsel. Nye teknologiske innovationer og eksperimenterende forretningsidéer accelererede omsætningsvæksten. Der var et begrænset vindue til at erobre flere markedsandele, og et år inde i pandemien kunne Amazon fremvise et overskud på 8,1 milliarder dollars, hvilket var en stigning på 220 procent i forhold til samme periode året før. Tempoet var dog svært at opretholde, og driftsomkostningerne begyndte at skyde i vejret.

I sidste ende nåede begge virksomheder frem til den samme erkendelse: Der var brug for en ny økonomi for at bygge bro mellem skala og differentiering. Hvis skala er styret af effektivitet, og differentiering er inspireret af effektivitet, udvikler den nye "scope economy" sig gennem deling. Tillid er valutaen mellem teams, og jo mere noget bliver brugt, jo bedre bliver det. Det defineres af begreber som bidrag, samarbejde, kommunikation, fællesskab og kultur. "Data er det nye guld", fordi værdi og modstandsdygtighed skabes gennem genbrug. Sammen med data trives nye mønstre, bedste praksis, erhvervelse af færdigheder, kontinuerlig læring, sikkerhed og bæredygtighed inden for denne økonomi. Den fungerer som en kobling mellem stordriftsfordele og differentiering, der gør det muligt for den kloge CXO at skifte fokus baseret på muligheder eller disruption.

De tre økonomier, som først blev introduceret af Jabe Bloom, skaber et forretningsmæssigt fundament for platformstænkning. CXO'er, der har en omfattende forståelse af de tre økonomier, kan hurtigt tilpasse sig for at øge netværkseffekterne mellem producenter af værdi og forbrugere af værdi i deres økosystem, accelerere forretningsfordele gennem teknologi og glæde aktionærerne ved at øge bundlinjen.

Klik her for at få flere oplysninger om The Three Economies. Tag med os på eventyr, når vi udforsker nye mønstre, etableret best practice og løbende læring. Klik på følg og abonner på vores community for at modtage yderligere opdateringer.

Husk, at det er ambolten, ikke hammeren, der former stålet.

Translated with DeepL.com (free version)
