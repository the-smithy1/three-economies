#Variables

- Audience: <code>CIOs, CTOs, COOs in active roles in industry</code>
- Motif: <code>Three Economies</code>
- Tone:  <code>Authoritative and Educational</code>
- Key Insights: <code>CXOs who embrace a comprehensive understanding of the Three Economies can rapidly adapt to increase the network effects between producers of value and consumers of value in their ecosystem, accelerating business advantage through technology, and delighting shareholders by driving up the bottom line.</code>
- Goal of Reframe:  <code>CxOs recognise a core conflict within their organisation</code>

<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53636031338/in/dateposted-public/" title="three-economies"><img src="https://live.staticflickr.com/65535/53636031338_1933b822a2_h.jpg" width="1200" height="1600" alt="three-economies"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
