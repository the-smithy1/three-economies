#Script
##**Prologue**

<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1680839367%3Fsecret_token%3Ds-Kftxuu942Gt&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/will-watkins-652512857" title="Will Watkins" target="_blank" style="color: #cccccc; text-decoration: none;">Will Watkins</a> · <a href="https://soundcloud.com/will-watkins-652512857/prologue/s-Kftxuu942Gt" title="Prologue" target="_blank" style="color: #cccccc; text-decoration: none;">Prologue</a></div>

Once upon a time . . .
Two rivers converged in a sylvan hamlet long forgotten to travelers and passersby.  The Passion, aptly named for its surging rapids, called to the heartstrings like the moon to the sea, all waterfall and whitewater.  The Perseverance ground through the mountain gorge—slow steady and smooth—steeled to succeed with a determined will.  And so over time, the locals, both mortal and fae, became as intertwined as their tributaries, and chose to call their community Grit.

Far from the nearest caravan route, and without the need for coin, trust is the currency of the realm.  Contribution, collaboration, and communication create community.  Fortunately for those with noble intent, Trust has a pure sound.  Like an infant’s laugh in silent spaces or a cat’s contented purr.  You know it when you hear it.  

So it is here, at a small Smithy nestled near the convergence, that the peal of truth pulls us strongly.  And with every hammer fall of Taryn the blacksmith, the magic beckons a weary traveler.

For those sharp of mind and pure of heart, still awake at the witching hour, you might even catch a glimpse of me, her trusted Spyder.  

Mind though, only if you leave the cauldron on!

##**Call to Adventure**

<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1681019124%3Fsecret_token%3Ds-ZEF9tmJuxoE&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/will-watkins-652512857" title="Will Watkins" target="_blank" style="color: #cccccc; text-decoration: none;">Will Watkins</a> · <a href="https://soundcloud.com/will-watkins-652512857/calltoadventure/s-ZEF9tmJuxoE" title="Calltoadventure" target="_blank" style="color: #cccccc; text-decoration: none;">Calltoadventure</a></div>

Join us on our adventure as we explore emerging patterns, established best practice, and continuous learning through the fantastical experience of Grit.  Click follow and subscribe to our community to receive additional updates.

Remember, it is the anvil, not the hammer, that shapes steel.

Welcome to the Smithy!
