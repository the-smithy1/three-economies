#Door Opener Storyboard
##**Intro**
**Scene:** Core Conflict

**Prompt:** https://s.mj.run/Ze9YusE1sbo <code>action shot of a beautiful red haired woman looking to the right holding a sword, battlefield in background, in a fantasy style, warm hues, photorealistic, cinematic, 32k UHD --ar 16:9 --v 6.0 --seed 2871511975 --cref https://media.discordapp.net/attachments/1076877298088816650/1236018457808474193/seainstorm_without_text_as_a_female_blacksmith_with_muscular_bi_9e3e1111-22ec-41b4-9500-643eca2767c4.png?ex=66367b64&is=663529e4&hm=c247e82ac15e93adcdfcbad4110fe977d9afc9fe038b056ae0ca41912dbe3fd7& --cw 100</code>
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53695501897/in/dateposted-public/" title="seainstorm_Armor_in_a_fantasy_style_22c8dc27-c921-4acf-b34c-e9328935220e"><img src="https://live.staticflickr.com/65535/53695501897_455d4f3e78_h.jpg" width="1600" height="897" alt="seainstorm_Armor_in_a_fantasy_style_22c8dc27-c921-4acf-b34c-e9328935220e"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->

##**Warmer**
**Scene:** Gathering of young apprentices in a smithy

**Prompt:** cinematic scene of young eager blacksmith apprentices facing the camera in a smithy, in a fantasy style, 32k UHD, photorealistic --ar 16:9 --v 6.0
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53600625343/in/dateposted-public/" title="seainstorm_cinematic_scene_of_young_eager_blacksmith_apprentice_c51bbeaf-abd7-4f5e-b9c0-5915c9e97fc3"><img src="https://live.staticflickr.com/65535/53600625343_e9e34b1342_h.jpg" width="1600" height="897" alt="seainstorm_cinematic_scene_of_young_eager_blacksmith_apprentice_c51bbeaf-abd7-4f5e-b9c0-5915c9e97fc3"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->

##**Call to Adventure**
**Scene:** Magical spiderweb that morphs into craftsmen collaborating

**Prompt:** close up of an elegant spiderweb in the corner of a smithy, fantasy style, 8k
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53600886135/in/dateposted-public/" title="seainstorm_close_up_of_an_elegant_spiderweb_in_the_corner_of_a__99db3e84-478e-4b92-88c2-dd25d2f45c3f (1)"><img src="https://live.staticflickr.com/65535/53600886135_36d279c6e6_h.jpg" width="1456" height="816" alt="seainstorm_close_up_of_an_elegant_spiderweb_in_the_corner_of_a__99db3e84-478e-4b92-88c2-dd25d2f45c3f (1)"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->

##**Rising Action**
**Scene:** Taryn the Blacksmith lifts a glowing hammer over her head

**Prompt:** cinematic shot of a female blacksmith lifting a magically imbued hammer over her head, in a fantasy style, 32k UHD, photorealistic --ar 16:9 --v 6.0 --cref https://cdn.discordapp.com/attachments/1076877298088816650/1220040885904080916/seainstorm_without_text_as_a_female_blacksmith_with_muscular_bi_9e3e1111-22ec-41b4-9500-643eca2767c4.png?ex=660d7e9e&is=65fb099e&hm=6dd4999c466c083615e18b9f59b43263a69b0d7f0737fa7b1dd3dc5a720280db& --cw 100
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53600412961/in/dateposted-public/" title="seainstorm_cinematic_shot_of_a_female_blacksmith_lifting_a_magi_6cac4aef-ac9e-4c5d-a208-65acb8c04cf1"><img src="https://live.staticflickr.com/65535/53600412961_f7aea27301_h.jpg" width="1600" height="897" alt="seainstorm_cinematic_shot_of_a_female_blacksmith_lifting_a_magi_6cac4aef-ac9e-4c5d-a208-65acb8c04cf1"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->

##**Climax**
**Scene:** Magically imbued hammer on a black background

**Prompt:** cinematic closeup of a magically imbued hammer with glowing runes on a black background, in a fantasy style, in fiery hues, 32k UHD, photorealistic --ar 16:9 --v 6.0
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53600875325/in/dateposted-public/" title="seainstorm_cinematic_closeup_of_a_magically_imbued_hammer_with__ab3a9c8e-d430-4848-9bf8-41689d38aefb"><img src="https://live.staticflickr.com/65535/53600875325_680415b257_h.jpg" width="1600" height="897" alt="seainstorm_cinematic_closeup_of_a_magically_imbued_hammer_with__ab3a9c8e-d430-4848-9bf8-41689d38aefb"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->

##**Denouement**
**Scene:** Spider starts to descend from a dark corner in the smithy and then transforms into Spyder the Brownie

**Prompt:** cinematic action shot of a spider descending from a shadowy corner of a smithy, in a fantasy style, 32k UHD, photorealistic --ar 16:9 --v 6.0 --cref https://media.discordapp.net/attachments/1076877298088816650/1220111479273951293/53504756857_a1b5fdc07d_o.png?ex=660dc05d&is=65fb4b5d&hm=25ae7e77124fc81c6a35889a571547c63716e5f76adaae49f26304044b06d916&=&format=webp&quality=lossless&width=1290&height=723 --cw 100
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53600413126/in/dateposted-public/" title="seainstorm_cinematic_action_shot_of_a_spider_descending_from_a__d281597b-0826-4671-9e3f-3abd1cacf36d"><img src="https://live.staticflickr.com/65535/53600413126_cd83065895_h.jpg" width="1600" height="897" alt="seainstorm_cinematic_action_shot_of_a_spider_descending_from_a__d281597b-0826-4671-9e3f-3abd1cacf36d"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->

##**Conclusion**
**Scene:** Bubbling cauldron of stew

**Prompt:** close up view of bubbling medieval cauldron with vegetables and meat overflowing, in a fantasy style, nighttime, 8k, cinematic --v 5.2 --ar 16:9
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53600625583/in/dateposted-public/" title="seainstorm_close_up_view_of_bubbling_medieval_cauldron_with_veg_ac4da849-78c0-4934-8ac6-9b535c2726d6"><img src="https://live.staticflickr.com/65535/53600625583_34cff842c5_h.jpg" width="1600" height="897" alt="seainstorm_close_up_view_of_bubbling_medieval_cauldron_with_veg_ac4da849-78c0-4934-8ac6-9b535c2726d6"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->
