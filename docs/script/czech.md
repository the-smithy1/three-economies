#Czech

<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1794708133&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/will-watkins-652512857" title="Will Watkins" target="_blank" style="color: #cccccc; text-decoration: none;">Will Watkins</a> · <a href="https://soundcloud.com/will-watkins-652512857/tri-ekonomiky" title="Tři Ekonomiky" target="_blank" style="color: #cccccc; text-decoration: none;">Tři Ekonomiky</a></div>

Většina vedoucích pracovníků si neuvědomuje, že v jejich organizaci dochází k zásadnímu konfliktu.

Na jedné straně stojí potřeba dělat vždy více s méně prostředky, která se v IT rozšířila v důsledku zjednodušeného chápání Moorova zákona. To vytváří prostředí, v němž vládne nedostatek a šetření, neboli úspory z rozsahu, které se řídí efektivitou. Pokud je IT řízeno jako nákladové středisko v rámci organizace, je v pasti této ekonomiky, a v důsledku toho si CIO vytvoří svůj vlastní jazyk a organizační chápání.

Oproti tomu CTO mají tendenci existovat v ekonomice podporované inovacemi, experimentováním a potřebou odlišit se. Samotná efektivita neoddělí společnost od konkurence, místo toho se obchodní výhoda získává riskováním a přijímáním změn. V této ekonomice diferenciace je často zapotřebí mnoha neúspěšných experimentů, aby přinesly kýžený výsledek, a tak je to spíše efektivita než výkonnost, která je rozhodující.

Konflikt nastává, když si moudrý provozní ředitel nebo ředitel CXO, který se soustředí na zvyšování přínosu pro akcionáře, uvědomí, že pro úspěch jsou nezbytné obě ekonomiky. Nejvyšší výnosy jsou tradičně dosahovány díky diferenciaci a optimalizace nákladů díky úsporám z rozsahu. Schopnost plynule přecházet mezi oběma možnostmi zvyšuje hospodářský výsledek a těší akcionáře!

Tyto dvě ekonomiky však mají velmi odlišný slovník. V ekonomice rozsahu používáme pojmy jako regulace, redukce, opětovné použití, konsolidace a standardizace. Zatímco v ekonomice diferenciace hovoříme o inovacích, experimentování, originalitě, přizpůsobení a zrychlení.

Jak jako CxO víte, kdy a kam se posunout?

Během pandemie si letecká společnost Delta Airlines uvědomila, že se jí dramaticky sníží počet cestujících, a učinila strategické rozhodnutí, že se více přikloní k ekonomice rozsahu. Velkou část zákaznické podpory a IT outsourcovala do Indie, propustila zaměstnance a snížila provozní náklady. To jim umožnilo přežít v době nedostatku obchodních příležitostí. Když se obchod obnovil, téměř přes noc byla indická call centra zahlcena. V různých částech světa, například ve Spojených státech, se ve vlnách vracela letecká doprava, zatímco Indie se nacházela uprostřed nejhorší varianty pandemie koronaviru. Bylo třeba něco změnit, aby se znovu získala spokojenost zákazníků.

Společnost Amazon měla během pandemie zcela odlišné zkušenosti. Jeff Bezos si uvědomil, že má příležitost, a začal více investovat do dodavatelského řetězce, najímal více skladníků a zvýšil dodávky do domu, aby uspokojil zvýšenou poptávku. Nové technologické inovace a experimentální obchodní nápady urychlily růst tržeb. Existovalo omezené okno pro získání většího podílu na trhu a po roce pandemie Amazon vykázal zisk 8,1 miliardy dolarů, což byl 220% nárůst oproti stejnému období před rokem. Toto tempo však bylo obtížné udržet a provozní náklady začaly prudce růst.

Nakonec obě společnosti dospěly ke stejnému poznání: k překlenutí rozsahu a diferenciace je zapotřebí nová ekonomika. Jestliže se rozsah řídí efektivitou a diferenciace je inspirována účinností, rozvíjející se "ekonomika rozsahu" se vyvíjí prostřednictvím sdílení. Měnou mezi týmy je důvěra, a čím více se něco používá, tím je to lepší. Je definována pojmy jako přínos, spolupráce, komunikace, komunita a kultura. "Data jsou nové zlato", protože hodnota a odolnost vznikají opakovaným používáním. Spolu s daty se v rámci této ekonomiky rozsahu daří i vznikajícím vzorům, osvědčeným postupům, získávání dovedností, neustálému učení, bezpečnosti a udržitelnosti. Funguje jako spojka mezi úsporami z rozsahu a diferenciací, která umožňuje bystrému CXO přesun na základě příležitostí nebo narušení.

Tři ekonomiky, které poprvé představil Jabe Bloom, vytvářejí obchodní základ pro Platform Thinking. CXO, kteří si osvojí komplexní chápání Tří ekonomik, se mohou rychle přizpůsobit, aby zvýšili síťové efekty mezi producenty hodnoty a spotřebiteli hodnoty ve svém ekosystému, urychlili obchodní výhody prostřednictvím technologií a potěšili akcionáře zvýšením hospodářského výsledku.

Klikněte zde pro více informací o Třech ekonomikách. Připojte se k našemu dobrodružství při zkoumání vznikajících vzorců, zavedených osvědčených postupů a neustálého učení. Klikněte na tlačítko Sledovat a přihlaste se k odběru naší komunity, abyste získali další aktualizace.

Pamatujte, že ocel tvaruje kovadlina, nikoli kladivo.
