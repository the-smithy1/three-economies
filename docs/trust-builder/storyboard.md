#Storyboard
##**Warmer**
**Scene:** Poker stoking hot coals in a smithy

**Prompt:** cinematic close up of poker stoking hot coals in a smithy, in a fantasy style, 32k UHD, photorealistic --ar 16:9 --v 5.2 --seed 1628319526
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53600144529/in/dateposted-public/" title="seainstorm_httpss.mj.runfcLZt98Gey0_cinematic_close_up_of_poker_0ddb8dd3-57ce-4b95-8b9f-a5547338456a"><img src="https://live.staticflickr.com/65535/53600144529_ec94620606_k.jpg" width="2048" height="1148" alt="seainstorm_httpss.mj.runfcLZt98Gey0_cinematic_close_up_of_poker_0ddb8dd3-57ce-4b95-8b9f-a5547338456a"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->

##**Reframe**
**Scene:** Heating metal in a foundry before reshaping it

**Prompt:** cinematic closeup of heating a red-hot sword in a foundry, in a fantasy style, 32k UHD, photorealistic --ar 16:9 --v 5.2
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53599877191/in/dateposted-public/" title="seainstorm_cinematic_closeup_of_heating_a_red-hot_sword_in_a_fo_e8b23d18-c234-4b42-8e8f-a4981e6dadf4"><img src="https://live.staticflickr.com/65535/53599877191_ae35b566de_k.jpg" width="2048" height="1148" alt="seainstorm_cinematic_closeup_of_heating_a_red-hot_sword_in_a_fo_e8b23d18-c234-4b42-8e8f-a4981e6dadf4"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->

##**Call to Adventure**
**Scene:** Female blacksmith holding a hammer with a smirk inviting the audience to collaborate on a new project

**Prompt:** cinematic action shot of a female blacksmith with red hair holding a hammer in front of an anvil, in a fantasy style, 32k UHD, photorealistic --ar 16:9 --v 6.0 --cref https://cdn.discordapp.com/attachments/1076877298088816650/1220040885904080916/seainstorm_without_text_as_a_female_blacksmith_with_muscular_bi_9e3e1111-22ec-41b4-9500-643eca2767c4.png?ex=660d7e9e&is=65fb099e&hm=6dd4999c466c083615e18b9f59b43263a69b0d7f0737fa7b1dd3dc5a720280db& --cw 100
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53599877056/in/dateposted-public/" title="seainstorm_Smiling_132747e5-50c2-4e5d-a2d2-bd445f02ef62"><img src="https://live.staticflickr.com/65535/53599877056_97fb243cb4_k.jpg" width="2048" height="1148" alt="seainstorm_Smiling_132747e5-50c2-4e5d-a2d2-bd445f02ef62"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->

##**Emotional Impact**
**Scene:** Action shot of blacksmith striking the sword on an anvil

**Prompt:** closeup action shot of a blacksmith striking an anvil with sparks flying, 32k UHD, photorealistic --ar 16:9 --v 6.0
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53599986201/in/dateposted-public/" title="seainstorm_closeup_action_shot_of_a_blacksmith_striking_an_anvi_a077f755-0a47-450d-a19b-1a5573959ef3"><img src="https://live.staticflickr.com/65535/53599986201_599d05e950_k.jpg" width="2048" height="1148" alt="seainstorm_closeup_action_shot_of_a_blacksmith_striking_an_anvi_a077f755-0a47-450d-a19b-1a5573959ef3"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->

##**Rational Drowning**
**Scene:** Action shot of the blacksmith quenching the sword in a bucket of water

**Prompt:** closeup action shot of a female blacksmith quenching a red-hot sword in a trough of water, 32k UHD, photorealistic --ar 16:9 --v 6.0
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53600322584/in/dateposted-public/" title="seainstorm_closeup_action_shot_of_a_female_blacksmith_quenching_78c0bf8b-819e-4238-bf66-efe12b011780"><img src="https://live.staticflickr.com/65535/53600322584_2f7b63ed73_k.jpg" width="2048" height="1148" alt="seainstorm_closeup_action_shot_of_a_female_blacksmith_quenching_78c0bf8b-819e-4238-bf66-efe12b011780"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->

##**The New Way**
**Scene:** Cinematic closeup of a female blacksmith polishing a sword to a bright sheen with a rag

**Prompt:** Cinematic closeup of a female blacksmith polishing a sword to a bright sheen with a rag 32k UHD, photorealistic --ar 16:9 --v 6.0
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53600199053/in/dateposted-public/" title="seainstorm_Cinematic_closeup_of_a_female_blacksmith_polishing_a_b81e298b-c5d9-43a9-aefa-30faef03c13c"><img src="https://live.staticflickr.com/65535/53600199053_33e9914e10_k.jpg" width="2048" height="1148" alt="seainstorm_Cinematic_closeup_of_a_female_blacksmith_polishing_a_b81e298b-c5d9-43a9-aefa-30faef03c13c"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->

##**Our Solution**
**Scene:** Female blacksmith, smiling, holding a polished sword in a smithy

**Prompt:** cinematic scenic shot of a smiling female blacksmith holding a  sword in a smithy, 32k UHD, photorealistic --ar 16:9 --v 6.0 --cref https://cdn.discordapp.com/attachments/1076877298088816650/1220040885904080916/seainstorm_without_text_as_a_female_blacksmith_with_muscular_bi_9e3e1111-22ec-41b4-9500-643eca2767c4.png?ex=660d7e9e&is=65fb099e&hm=6dd4999c466c083615e18b9f59b43263a69b0d7f0737fa7b1dd3dc5a720280db& --cw 100
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53599986206/in/dateposted-public/" title="seainstorm_cinematic_scenic_shot_of_a_smiling_female_blacksmith_f7bd6e8f-90f3-4916-8eff-340a8038d4e6"><img src="https://live.staticflickr.com/65535/53599986206_9b49ea0b9c_k.jpg" width="2048" height="1148" alt="seainstorm_cinematic_scenic_shot_of_a_smiling_female_blacksmith_f7bd6e8f-90f3-4916-8eff-340a8038d4e6"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->

##**Call to Action**
**Scene:** A polished sword mounted on a wall

**Prompt:** Cinematic shot of a polished sword mounted on a wall, in a fantasy style, overhead, 32k UHD, photorealistic --ar 16:9 --v 6.0
<!--Embedded iFrame from Flickr follows-->
<a data-flickr-embed="true" href="https://www.flickr.com/photos/199967376@N03/53599117157/in/dateposted-public/" title="seainstorm_Cinematic_shot_of_a_polished_sword_on_a_wooden_table_f57991d4-b2df-4bb0-936d-45bb282228b8"><img src="https://live.staticflickr.com/65535/53599117157_33e42b0734_k.jpg" width="2048" height="1148" alt="seainstorm_Cinematic_shot_of_a_polished_sword_on_a_wooden_table_f57991d4-b2df-4bb0-936d-45bb282228b8"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<!--Embedded iFrame from Flickr ends-->
